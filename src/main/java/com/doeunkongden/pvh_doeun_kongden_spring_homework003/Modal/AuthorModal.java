package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class AuthorModal {
    private Integer author_id;
    private String author_name;
    private String author_gender;
}
