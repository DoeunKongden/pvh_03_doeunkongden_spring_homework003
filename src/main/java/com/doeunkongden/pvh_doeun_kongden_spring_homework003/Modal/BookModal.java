package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal;


import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.AuthorRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookModal {
    private Integer book_id;
    private String book_title;
    private LocalDateTime published_date;
    private AuthorModal authorModal;
    private List<CategoryModal> categoryModalList;
}
