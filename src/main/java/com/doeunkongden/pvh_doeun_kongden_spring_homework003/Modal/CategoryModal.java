package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryModal {
    private int category_id;
    private String category_name;
}
