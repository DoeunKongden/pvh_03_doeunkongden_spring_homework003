package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthorRequest {
    private String author_name;
    private String author_gender;
}
