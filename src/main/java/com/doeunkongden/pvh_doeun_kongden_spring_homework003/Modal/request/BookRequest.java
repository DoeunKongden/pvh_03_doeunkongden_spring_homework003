package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookRequest {
    private String book_title;
    private LocalDateTime published_date;
    private Integer author_id;
    private List<Integer> category_id;
}
