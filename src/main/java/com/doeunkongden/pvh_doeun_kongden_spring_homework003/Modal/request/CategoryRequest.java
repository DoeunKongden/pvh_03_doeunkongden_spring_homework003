package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CategoryRequest {
    private String category_name;
}
