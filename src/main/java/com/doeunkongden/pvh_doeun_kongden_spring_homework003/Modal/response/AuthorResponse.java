package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AuthorResponse<T> {
    private T payload;
    private String message;
    private Integer statusCode;
    private LocalDateTime timestamp;
}
