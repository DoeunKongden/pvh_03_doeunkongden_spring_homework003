package com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookResponse<T> {
    private T payload;
    private String message;
    private Integer statusCode;
    private LocalDateTime timestamp;
}
