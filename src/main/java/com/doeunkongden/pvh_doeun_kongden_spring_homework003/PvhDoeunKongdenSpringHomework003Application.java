package com.doeunkongden.pvh_doeun_kongden_spring_homework003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PvhDoeunKongdenSpringHomework003Application {

    public static void main(String[] args) {
        SpringApplication.run(PvhDoeunKongdenSpringHomework003Application.class, args);
    }

}
