package com.doeunkongden.pvh_doeun_kongden_spring_homework003.controllers;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.AuthorModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.AuthorRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response.AuthorResponse;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response.CategoryResponse;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.AuthorService;
import org.springframework.cglib.core.Local;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @PostMapping("post-new-author")
    ResponseEntity<AuthorResponse<AuthorModal>> insertNewAuthor(@RequestBody AuthorRequest authorRequest) {
        AuthorResponse<AuthorModal> authorResponse = AuthorResponse.<AuthorModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Succesfully Add New Author")
                .payload(authorService.insertNewAuthro(authorRequest))
                .build();
        return ResponseEntity.ok().body(authorResponse);
    }

    @GetMapping("get-all-author")
    ResponseEntity<AuthorResponse<List<AuthorModal>>> getAllCategory() {
        AuthorResponse<List<AuthorModal>> authorResponse = AuthorResponse.<List<AuthorModal>>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Succesfully Fetch All Author")
                .payload(authorService.getAllAuthor())
                .build();
        return ResponseEntity.ok().body(authorResponse);
    }

    @GetMapping("get-author-by-id/{id}")
    ResponseEntity<AuthorResponse<AuthorModal>> getAuthorById(@PathVariable("id") Integer authorId) {
        AuthorResponse<AuthorModal> authorResponse = AuthorResponse.<AuthorModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .payload(authorService.getAuthorById(authorId))
                .message("Successfully Fetch Author")
                .build();
        return ResponseEntity.ok().body(authorResponse);
    }

    @DeleteMapping("delete-author-by-id/{id}")
    ResponseEntity<AuthorResponse<AuthorModal>> deleteAuthorById(@PathVariable("id") Integer authorId) {
        AuthorResponse<AuthorModal> authorResponse = AuthorResponse.<AuthorModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .payload(authorService.deleteAuthorById(authorId))
                .message("Succsufully Delete Author")
                .build();
        return ResponseEntity.ok().body(authorResponse);
    }

    @PutMapping("update-author-by-id/{id}")
    ResponseEntity<AuthorResponse<AuthorModal>> updateAuthorById(@RequestBody AuthorRequest authorRequest,@PathVariable("id") Integer authorId){
        AuthorResponse<AuthorModal> authorResponse = AuthorResponse.<AuthorModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .payload(authorService.updateAuthorById(authorRequest,authorId))
                .message("Successfully Update User")
                .build();
        return ResponseEntity.ok().body(authorResponse);
    }
}
