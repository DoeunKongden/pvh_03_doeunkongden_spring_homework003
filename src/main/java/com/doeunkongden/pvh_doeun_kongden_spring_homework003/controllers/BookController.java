package com.doeunkongden.pvh_doeun_kongden_spring_homework003.controllers;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.BookModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.BookRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response.BookResponse;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository.BookRepository;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.BookService;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Result;
import org.springframework.cglib.core.Local;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class BookController {


    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }


    @GetMapping("get-all-book")
    ResponseEntity<BookResponse<List<BookModal>>> getAllBook(){
        BookResponse<List<BookModal>> bookResponse = BookResponse.<List<BookModal>>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Successfully Fetch All Book")
                .payload(bookService.getAllBook())
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

    @GetMapping("get-book-by-id/{id}")
    ResponseEntity<BookResponse<BookModal>> getBookById(@PathVariable("id")Integer book_id){
        BookResponse<BookModal> bookResponse = BookResponse.<BookModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Successfully Fetch Book Detail")
                .payload(bookService.getBookById(book_id))
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

    @PostMapping("post-new-book")
    ResponseEntity<BookResponse<BookModal>> getBookById(@RequestBody BookRequest bookRequest){
        Integer book_id = bookService.postNewBook(bookRequest);
        BookModal bookModal = bookService.getBookById(book_id);

        BookResponse<BookModal> bookResponse = BookResponse.<BookModal>builder()
                .message("Post New Book Successfully")
                .payload(bookModal)
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

    @DeleteMapping("delete-book-by-id/{id}")
    public ResponseEntity<BookResponse<BookModal>> deleteBookById(@PathVariable("id")Integer book_id){
        BookResponse<BookModal> bookResponse = BookResponse.<BookModal>builder()
                .message("Delete Book Successfully")
                .payload(bookService.deleteBookById(book_id))
                .timestamp(LocalDateTime.now())
                .statusCode(200)
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

    @PutMapping("update-book-by-id/{id}")
    public ResponseEntity<BookResponse<BookModal>> updateBookId(@PathVariable("id")Integer book_id, @RequestBody BookRequest bookRequest){
        Integer bookReturnId  = bookService.updateBookById(book_id, bookRequest);
        BookModal bookModal = bookService.getBookById(bookReturnId);
        BookResponse<BookModal> bookResponse = BookResponse.<BookModal>builder()
                .message("Successfully Update Book")
                .payload(bookModal)
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

}
