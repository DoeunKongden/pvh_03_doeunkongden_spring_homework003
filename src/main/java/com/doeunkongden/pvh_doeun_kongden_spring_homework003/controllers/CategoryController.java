package com.doeunkongden.pvh_doeun_kongden_spring_homework003.controllers;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.CategoryRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response.CategoryResponse;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.CategoryService;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("api/v1/")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("get-all-category")
    ResponseEntity<CategoryResponse<List<CategoryModal>>> getAllCategory(){
        CategoryResponse<List<CategoryModal>> categoryResponse = CategoryResponse.<List<CategoryModal>>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Succesfully Fetch All Category")
                .payload(categoryService.getAllCategory())
                .build();
        return ResponseEntity.ok().body(categoryResponse);
    }

    @GetMapping("get-category-by-id/{id}")
    ResponseEntity<CategoryResponse<CategoryModal>> getCategoryById(@PathVariable("id")Integer category_id){
        CategoryResponse<CategoryModal> categoryResponse = CategoryResponse.<CategoryModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Succesfully Fetch One Category")
                .payload(categoryService.getCategoryById(category_id))
                .build();
        return ResponseEntity.ok().body(categoryResponse);
    }

    @PutMapping("update-category-by-id/{id}")
    ResponseEntity<CategoryResponse<CategoryModal>> updateCategoryById(@RequestBody CategoryRequest categoryRequest,@PathVariable("id") Integer category_id){
        CategoryResponse<CategoryModal> categoryResponse = CategoryResponse.<CategoryModal>builder()
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .message("Update Category Successful")
                .payload(categoryService.updateCategoryById(categoryRequest,category_id))
                .build();

        return ResponseEntity.ok().body(categoryResponse);
    }

    @PostMapping("post-new-category")
    ResponseEntity<CategoryResponse<CategoryModal>> insertNewCategory(@RequestBody CategoryRequest categoryRequest){
        CategoryResponse<CategoryModal> categoryResponse = CategoryResponse.<CategoryModal>builder()
                .payload(categoryService.insertNewCategory(categoryRequest))
                .message("Successfully Added New Category")
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(categoryResponse);
    }

    @DeleteMapping("delete-category-by-id/{id}")
    ResponseEntity<CategoryResponse<CategoryModal>> deleteCategoryById(@PathVariable("id") Integer id){
        CategoryResponse<CategoryModal> categoryResponse = CategoryResponse.<CategoryModal>builder()
                .payload(categoryService.deleteCategoryById(id))
                .message("Delete Category Successful")
                .statusCode(200)
                .timestamp(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(categoryResponse);
    }
}
