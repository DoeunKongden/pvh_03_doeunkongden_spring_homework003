package com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception;

public class EmptryFieldOrInvalidException extends RuntimeException{
    public EmptryFieldOrInvalidException(String message){
        super(message);
    }
}
