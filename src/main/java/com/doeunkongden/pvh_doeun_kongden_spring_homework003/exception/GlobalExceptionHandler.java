package com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EmptryFieldOrInvalidException.class)
    ProblemDetail emptyFieldHandler(EmptryFieldOrInvalidException exception){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST,exception.getMessage()
        );
        problemDetail.setTitle("Empty Or Invalid Field");
        problemDetail.setType(URI.create("localhost:8080/error/bad-request"));
        return problemDetail;
    }

    @ExceptionHandler(NullException.class)
    ProblemDetail nullExceptionHandler(NullException nullException){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND,nullException.getMessage()
        );
        problemDetail.setTitle("Null Field Exception");
        problemDetail.setType(URI.create("localhost:8080/error/null-data"));
        return problemDetail;
    }
}
