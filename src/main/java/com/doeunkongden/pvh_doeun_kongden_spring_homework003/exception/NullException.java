package com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception;

public class NullException extends RuntimeException{
    public NullException(String message) {
        super(message);
    }
}
