package com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.AuthorModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {

    @Select("""
            insert into author_tb(author_name,author_gender) values (#{authorRequest.author_name},#{authorRequest.author_gender}) returning *
            """)
    @Results(id = "authorMap", value = {
            @Result(property = "author_id", column = "author_id"),
            @Result(property = "author_name", column = "author_name"),
            @Result(property = "author_gender", column = "author_gender")
    })
    AuthorModal insertNewAuthor(@Param("authorRequest") AuthorRequest authorRequest);

    @Select("""
            select * from author_tb
            """)
    @ResultMap(("authorMap"))
    List<AuthorModal> getAllAuthor();

    @Select("""
            select * from author_tb where author_id =#{authorId}
            """)
    @ResultMap("authorMap")
    AuthorModal getAuthorById(Integer authorId);


    @Select("""
            delete from author_tb where author_id = #{authorId} returning *;
            """)
    @ResultMap("authorMap")
    AuthorModal deleteAuthorById(Integer authorId);


    @Select("""
            update author_tb set author_name=#{authorRequest.author_name} ,author_gender=#{authorRequest.author_gender} where author_id = #{authorId} returning *;
            """)
    @ResultMap("authorMap")
    AuthorModal updateAuthorById(@Param("authorRequest") AuthorRequest authorRequest, Integer authorId);
}
