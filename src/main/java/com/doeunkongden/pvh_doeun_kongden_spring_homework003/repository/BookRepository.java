package com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.BookModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {


    @Select("""
            select * from book_tb
            """)
    @Results(id = "mapBook", value = {
            @Result(property = "book_id", column = "book_id"),
            @Result(property = "book_title", column = "book_title"),
            @Result(property = "published_date", column = "published_date"),
            @Result(property = "authorModal", column = "author_id", one = @One(select = "com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository.AuthorRepository.getAuthorById")),
            @Result(property = "categoryModalList", column = "book_id", many = @Many(select = "getAllCategoryById"))
    })
    List<BookModal> getAllBook();

    @Select("""
            select category_name,category_id from bookdetails_tb inner join bookcategories_tb bt on bt.category_id = bookdetails_tb.cateory_id where book_id = #{book_id}
            """)
    @Result(property = "category_id",column = "category_id")
    @Result(property = "category_name",column = "category_name")
    List<CategoryModal> getAllCategoryById(Integer book_id);

    @Select("""
            select * from book_tb where book_id = #{bookId}
            """)
    @ResultMap("mapBook")
    BookModal getBookById(Integer bookId);


    @Select("""
            insert into book_tb(book_title, published_date, author_id) values (#{bookRequest.book_title},#{bookRequest.published_date},#{bookRequest.author_id}) returning book_id
            """)
    Integer postNewBook(@Param("bookRequest") BookRequest bookRequest);


    @Select("""
              insert into bookdetails_tb(book_id,cateory_id) values (#{book_id},#{category_id})
            """)
    void addCategoryToCategoriDetail(Integer book_id, Integer category_id);


    @Delete("delete from bookdetails_tb where book_id =#{book_id}")
    void deleteBookInBookDetail(Integer book_id);

    @Select("delete from book_tb where book_id = #{bookId} returning *")
    @ResultMap("mapBook")
    BookModal deleteBookById(Integer bookId);

    @Select("update book_tb set author_id = #{bookRequest.author_id}, book_title = #{bookRequest.book_title} where book_id = #{bookId} returning book_id")
    Integer updateBookById(Integer bookId, @Param("bookRequest") BookRequest bookRequest);
}
