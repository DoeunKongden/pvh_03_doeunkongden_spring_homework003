package com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("""
            insert into bookcategories_tb(category_name) values (#{categoryRequest.category_name}) returning *;
            """)
    CategoryModal insertNewCategory(@Param("categoryRequest") CategoryRequest categoryRequest);

    @Select(("""
             select * from bookcategories_tb
            """))
    @Results(id = "categoryMap", value = {
            @Result(property = "category_id", column = "category_id"),
            @Result(property = "category_name", column = "category_name")
    })
    List<CategoryModal> getAllCategory();


    @Select("""
            delete from bookcategories_tb where category_id = #{categoryId} returning *
            """)
    @ResultMap("categoryMap")
    CategoryModal deleteCategoryById(Integer categoryId);

    @Select("""
            update bookcategories_tb set category_name=#{categoryRequest.category_name} where category_id=#{category_id} returning * 
            """)
    @ResultMap("categoryMap")
    CategoryModal updateCategoryById(@Param("categoryRequest") CategoryRequest categoryRequest, Integer category_id);

    @Select("""
            Select * from bookcategories_tb where category_id = #{categoryId}
            """)
    CategoryModal getCategoryById(Integer categoryId);
}
