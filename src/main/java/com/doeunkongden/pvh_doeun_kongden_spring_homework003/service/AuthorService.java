package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.AuthorModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    AuthorModal insertNewAuthro(AuthorRequest authorRequest);
    List<AuthorModal> getAllAuthor();
    AuthorModal getAuthorById(Integer author_id);
    AuthorModal deleteAuthorById(Integer author_id);

    AuthorModal updateAuthorById(AuthorRequest authorRequest, Integer author_id);

}
