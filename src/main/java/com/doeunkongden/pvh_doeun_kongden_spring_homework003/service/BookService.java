package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.BookModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.BookRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.response.BookResponse;

import java.awt.print.Book;
import java.util.List;

public interface BookService {
    List<BookModal> getAllBook();
    BookModal getBookById(Integer book_id);
    Integer postNewBook(BookRequest bookRequest);
    BookModal deleteBookById(Integer book_id);
    Integer updateBookById(Integer book_id,BookRequest bookRequest);
}
