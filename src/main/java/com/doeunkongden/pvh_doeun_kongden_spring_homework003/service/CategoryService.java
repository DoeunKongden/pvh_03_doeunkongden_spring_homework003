package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.CategoryRequest;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CategoryService {
    CategoryModal insertNewCategory(CategoryRequest categoryRequest);
    List<CategoryModal> getAllCategory();
    CategoryModal deleteCategoryById(Integer category_id);
    CategoryModal getCategoryById(Integer category_id);
    CategoryModal updateCategoryById(CategoryRequest categoryRequest,Integer category_id);
}
