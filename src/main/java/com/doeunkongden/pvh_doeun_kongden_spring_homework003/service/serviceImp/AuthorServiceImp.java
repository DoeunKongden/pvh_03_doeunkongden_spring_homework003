package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.serviceImp;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.AuthorModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.AuthorRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.EmptryFieldOrInvalidException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.NullException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository.AuthorRepository;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public AuthorModal insertNewAuthro(AuthorRequest authorRequest) {

        AuthorModal authorModal = authorRepository.insertNewAuthor(authorRequest);

        String regex = "(0/91)?[7-9][0-9]{9}";


        if (authorRequest.getAuthor_name().isEmpty() || authorRequest.getAuthor_name().isBlank() || authorRequest.getAuthor_name().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Name Input");
        } else if (authorRequest.getAuthor_gender().isBlank() || authorRequest.getAuthor_gender().isEmpty() || authorRequest.getAuthor_gender().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Gender Input");
        } else if (authorRequest.getAuthor_name().length() < 4 || authorRequest.getAuthor_gender().length() < 4) {
            throw new EmptryFieldOrInvalidException("Gender And Name Must Be Over 4 Character");
        } else if (!(authorRequest.getAuthor_gender().equalsIgnoreCase("male")) || !(authorRequest.getAuthor_gender().equalsIgnoreCase("female"))) {
            throw new EmptryFieldOrInvalidException("Gender Must Be : Male || Female");
        } else if (authorRequest.getAuthor_name().matches(regex) || authorRequest.getAuthor_gender().matches(regex)) {
            throw new EmptryFieldOrInvalidException("Author Name And Gender Can not contain Number");
        } else {
            return authorModal;
        }
    }

    @Override
    public List<AuthorModal> getAllAuthor() {
        if (authorRepository.getAllAuthor() == null) {
            throw new NullException("There Is No Data For Author");
        } else {
            return authorRepository.getAllAuthor();
        }
    }

    @Override
    public AuthorModal getAuthorById(Integer author_id) {
        if (authorRepository.getAuthorById(author_id) == null) {
            throw new NullException("There Is No Data For ID : " + author_id);
        } else {
            return authorRepository.getAuthorById(author_id);
        }
    }

    @Override
    public AuthorModal deleteAuthorById(Integer author_id) {
        if (authorRepository.deleteAuthorById(author_id) == null) {
            throw new NullException("This Author ID : " + author_id + "Don't Exist");
        }
        return authorRepository.deleteAuthorById(author_id);
    }

    @Override
    public AuthorModal updateAuthorById(AuthorRequest authorRequest, Integer author_id) {

        String regex = "(0/91)?[7-9][0-9]{9}";

        if (authorRepository.updateAuthorById(authorRequest, author_id) == null) {
            throw new NullException("There Is No Data For ID : " + author_id);
        } else if (!(authorRequest.getAuthor_gender().equalsIgnoreCase("male")) || !(authorRequest.getAuthor_gender().equalsIgnoreCase("female"))) {
            throw new EmptryFieldOrInvalidException("Author Gender Must Be : Male || Female");
        } else if (authorRequest.getAuthor_name().isEmpty() || authorRequest.getAuthor_name().isBlank() || authorRequest.getAuthor_name().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Author Input");
        } else if (authorRequest.getAuthor_name().length() < 4 || authorRequest.getAuthor_gender().length() < 4) {
            throw new EmptryFieldOrInvalidException("Invalid Input Data");
        } else if (authorRequest.getAuthor_name().matches(regex) || authorRequest.getAuthor_gender().matches(regex)) {
            throw new EmptryFieldOrInvalidException("Author Gender And Name Can Not Contain Number");
        } else {
            return authorRepository.updateAuthorById(authorRequest, author_id);
        }
    }
}
