package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.serviceImp;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.BookModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.BookRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.EmptryFieldOrInvalidException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.NullException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository.BookRepository;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookServiceImp implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookModal> getAllBook() {
        if (bookRepository.getAllBook() == null) {
            throw new NullException("There Is No Book Data To Get");
        } else {
            return bookRepository.getAllBook();
        }
    }

    @Override
    public BookModal getBookById(Integer book_id) {
        if (bookRepository.getBookById(book_id) == null) {
            throw new NullException("Book ID : " + book_id + "Don't Exist");
        } else if (book_id < 0) {
            throw new EmptryFieldOrInvalidException("Book ID Can Not Be Negative Number");
        } else {
            return bookRepository.getBookById(book_id);
        }
    }

    @Override
    public Integer postNewBook(BookRequest bookRequest) {

        Integer bookId = bookRepository.postNewBook(bookRequest);

        if (bookRequest.getBook_title().isBlank() || bookRequest.getBook_title().isEmpty() || bookRequest.getBook_title().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Book Title Input");
        } else {
            for (int i = 0; i < bookRequest.getCategory_id().size(); i++) {
                bookRepository.addCategoryToCategoriDetail(bookId, bookRequest.getCategory_id().get(i));
            }
            return bookId;
        }
    }

    @Override
    public BookModal deleteBookById(Integer book_id) {
        if (getBookById(book_id) == null) {
            throw new NullException("Book ID : " + book_id + "Don't Exist");
        } else if (book_id < 0) {
            throw new EmptryFieldOrInvalidException("Book ID Can Not Be Negative Number");
        } else {
            return bookRepository.deleteBookById(book_id);
        }
    }

    @Override
    public Integer updateBookById(Integer book_id, BookRequest bookRequest) {
        Integer bookReturnId = bookRepository.updateBookById(book_id, bookRequest);

        if (bookRequest.getBook_title().equalsIgnoreCase("string") || bookRequest.getBook_title().isEmpty() || bookRequest.getBook_title().isBlank()) {
            throw new EmptryFieldOrInvalidException("Invalid Book Title Input");
        } else if (getBookById(book_id) == null) {
            throw new NullException("Book ID " + book_id + "Don't Exist");
        } else if (book_id < 0) {
            throw new EmptryFieldOrInvalidException("Book Id Can not be negative numebr");
        } else {
            bookRepository.deleteBookInBookDetail(bookReturnId);
            for (int i = 0; i < bookRequest.getCategory_id().size(); i++) {
                bookRepository.addCategoryToCategoriDetail(bookReturnId, bookRequest.getCategory_id().get(i));
            }
            return bookRepository.updateBookById(book_id, bookRequest);
        }
    }
}
