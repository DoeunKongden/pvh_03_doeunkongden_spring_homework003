package com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.serviceImp;

import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.CategoryModal;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.Modal.request.CategoryRequest;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.EmptryFieldOrInvalidException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.exception.NullException;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.repository.CategoryRepository;
import com.doeunkongden.pvh_doeun_kongden_spring_homework003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private final CategoryRepository categocryRepository;

    public CategoryServiceImp(CategoryRepository categocryRepository) {
        this.categocryRepository = categocryRepository;
    }

    @Override
    public CategoryModal insertNewCategory(CategoryRequest categoryRequest) {

        String regex = "(0/91)?[7-9][0-9]{9}";

        CategoryModal categoryModal = categocryRepository.insertNewCategory(categoryRequest);
        if (categoryRequest.getCategory_name().isBlank() || categoryRequest.getCategory_name().isEmpty() || categoryRequest.getCategory_name().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Category Name");
        } else if (categoryRequest.getCategory_name().length() < 4) {
            throw new EmptryFieldOrInvalidException("Category Name Must Be More Then 4 Character");
        } else if (categoryRequest.getCategory_name().matches(regex)) {
            throw new EmptryFieldOrInvalidException("Category Name Can Not Contain Number");
        } else {
            return categoryModal;
        }
    }

    @Override
    public List<CategoryModal> getAllCategory() {
        if (categocryRepository.getAllCategory() == null) {
            throw new NullException("There Is No Data For Category");
        } else {
            return categocryRepository.getAllCategory();
        }
    }

    @Override
    public CategoryModal deleteCategoryById(Integer category_id) {
        if (categocryRepository.deleteCategoryById(category_id) == null) {
            throw new NullException("There Is No Data To Delete : " + category_id);
        } else if (category_id < 0) {
            throw new EmptryFieldOrInvalidException("Category ID Can't Be Negative Number");
        } else {
            return categocryRepository.deleteCategoryById(category_id);
        }
    }

    @Override
    public CategoryModal getCategoryById(Integer category_id) {
        if (categocryRepository.getCategoryById(category_id) == null) {
            throw new NullException("Category ID : " + category_id + "Don't Exist");
        } else if (category_id < 0) {
            throw new EmptryFieldOrInvalidException("ID Of Category Can't Be Negative Number");
        } else {
            return categocryRepository.getCategoryById(category_id);
        }
    }

    @Override
    public CategoryModal updateCategoryById(CategoryRequest categoryRequest, Integer category_id) {

        String regex = "(0/91)?[7-9][0-9]{9}";

        CategoryModal categoryModal = categocryRepository.updateCategoryById(categoryRequest, category_id);
        if (categoryRequest.getCategory_name().isEmpty() || categoryRequest.getCategory_name().isBlank() || categoryRequest.getCategory_name().equalsIgnoreCase("string")) {
            throw new EmptryFieldOrInvalidException("Invalid Category Name");
        } else if (categoryRequest.getCategory_name().length() < 4) {
            throw new EmptryFieldOrInvalidException("Category Name Must Be More Then 4 Character");
        } else if (category_id < 0) {
            throw new EmptryFieldOrInvalidException("ID Of Category Can't Be Negative Number");
        } else if(categocryRepository.updateCategoryById(categoryRequest,category_id) == null){
            throw new NullException("There category ID : "+category_id+"Don't Exist");
        }
        else if(categoryRequest.getCategory_name().matches(regex)){
            throw new EmptryFieldOrInvalidException("Category Name Can Not Contain Number");
        }
        else {
            return categoryModal;
        }
    }

}
